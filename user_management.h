#ifndef USER_MANAGEMENT_GUARD__H
#define USER_MANAGEMENT_GUARD__H

#include <stdio.h>

// Macro for the user info
#define MaxName 100     //Array size for name
#define MaxEmail 100     //Array size for email
#define MaxUsername 100      //Array size for username
#define MaxPassword 100    //Array size for password


// Macro for the book info
#define MaxBook 100         //Array size for book in library
#define MaxBookName 100      //Array size for book in bookname
#define MaxAuthorName 100    //Array size for author's name

// Macro for filename
#define fileUser "User.bin"      //file that stores user informations 
#define fileLibrary "Library.bin"     //file that stores all books in library
#define fileBook "Book.bin"       //file that stores current book added by admin
#define fileLoan "BookLoan.bin"     //file that stores borrow and return book transaction


typedef struct
{
    char name [MaxName];    //User's first name
    char email [MaxEmail];    //User's email
    char username [MaxUsername];    //Username for login
    char password [MaxPassword];    //Password for login
} UserRegister;


//a struct that contains user and book information
//for borrow and return function
typedef struct
{
    char name [MaxName];     //Current user's name
    char bookTitle [MaxBookName];   //Book Tittle
    char bookAuthor [MaxAuthorName];    //Author's name
    int year;   //Book year of production
} LoanStatus;


//search book in fileLibrary by title
//if found, and not currently on loan
//user and book details is saved into struct LoanStatus
//struct LoanStatus will be written into the fileLoan if the book successfully borrowed
//library is being updated after borrow book, copies reduce by 1
void borrow_book();


//search book in fileLoan by title
//if found, user can return book
//restrict user to return the same book more than once
//The returned book will be removed from fileLoan
//library is being updated after return book, copies increase by 1
void return_book();


#endif


