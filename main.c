#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "book_management.h"
#include "user_management.h"

static struct Book book;     // Declare book of type Book
char sessionName[MaxName];         // Declare current session user's name
const char adminUsername[] = "admin";    // Declare constant for admin username


//print out library management system welcome message
//get user input to proceed
void headerBanner()
{
    printf("\n ****************************************** ");
    printf("\n                                          ");
    printf("\n             LIBRARY MANAGEMENT           ");
    printf("\n                 SYSTEM                   ");
    printf("\n                                          ");
    printf("\n ****************************************** ");
    printf("\n\n Press enter to proceed..");
    getchar();
}


//function that calls three struct BookArray find_book functions
//to find book by book title, author's name or year of production
void findBooks()
{
    char bookName[MaxBookName] ;
    char authorName[MaxAuthorName];
    int bookyear;
    int choice = 0;
    printf("\n\n\n Please choose how you want to search book");
    printf("\n\n\n1.By book title");
    printf("\n2.By book author");
    printf("\n3.By year");
    printf("\n0.Exit");
    printf("\n\n\n Please enter a choice => ");
    scanf("%d",&choice);
    switch(choice)
        {
        case 1:
            printf("\nEnter Book Title to search: ");
            int c;
            while ((c = getchar()) != '\n' && c != EOF);
            fgets(bookName ,MaxBookName,stdin);
            find_book_by_title (bookName);
            break;
        case 2:
            printf("\nEnter Author's name to search: ");
            while ((c = getchar()) != '\n' && c != EOF);
            fgets(authorName ,MaxAuthorName,stdin);
            find_book_by_author(authorName);
            break;
        case 3:
            printf("\nEnter Year of Publication to search: ");
            scanf("%i",&bookyear);
  	    if(isdigit(bookyear)){
  	        printf("\nPlease enter numeric value only");
		        }
            find_book_by_year(bookyear);
            break;
        case 0:
            printf("\n\n\n See you again \n\n\n\n\n");
            exit(1);
            break;
        default:
            printf("\n\n\n Invalid Input. Try again.");
        }
}


//check if file exists
//if file has existed, return 1 as variable a, else return 0;
int checkFile(const char *open)
{
    int a = 0;
    FILE *file = fopen(open, "rb");

    if (file != NULL)
    {
        a = 1;
        fclose(file);
    }
    return a;
}


//create file if fileUser does not exist
void createFileUser()
{
    int a = 0;
    a = checkFile(fileUser);

    FILE *file = NULL;
    if(!a)
    {
        file = fopen(fileUser,"wb");
        if(file != NULL)
        {
            fclose(file);
        }
    }
}


//create file if fileLoan does not exist
void createFileLoan()
{
    int a = 0;
    a = checkFile(fileLoan);

    FILE *file = NULL;
    if(!a)
    {
        file = fopen(fileLoan,"wb");
        if(file != NULL)
        {
            fclose(file);
        }
    }
}


//Validate email address consist '@'
//return valid as return 1, else return 0;
int emailCheck(char email[])
{
    int valid = 0;
    int length = strlen(email);

    for(int i = 0; i < length; i++)
    {
        if (email[i] == '@')
        {
            valid = 1;
        }
    }
    if ( valid == 0)
    {
        printf("\n\n Email not valid. Please include '@' in your email");
    }
    return valid;
}


//Request user to input personal information
//validate that email contains '@', else output error message
//username and password is not the same as admin's, else output error message
//if successfully registered, the details is saved into fileUser
void registerUser(void)
{
    UserRegister info = {0};
    FILE *file = NULL;
    char name[MaxName] = {0};
    char email[MaxEmail]= {0};
    char username[MaxUsername] = {0};
    char password[MaxPassword] = {0};


    file = fopen(fileUser,"a+b");
    if(file == NULL)
    {
        printf("\n\n\n File is not opened\n");
       exit(1);
    }
    printf("\n\n\n---Please Do Not Try To Input Multiple Words For Each Detail---");
    printf("\n\n First Name :");
    scanf("%s", name);
    do{
    printf("\n\n Email :");
    scanf("%s", email);
    }
    while(emailCheck(email)!=1);
    printf("\n\n Username:");
    scanf("%s",username);
    if(!strcmp(username,adminUsername)){
    printf("\n Username not allowed. Try again");
    printf("\n\n Username:");
    scanf("%s",username);
    }
    printf("\n\n Password:");
    scanf("%s", password);
    if(!strcmp(password,adminUsername)){
    printf("\n Password not allowed. Try again");
    printf("\n\n Password:");
    scanf("%s", password);
    }
    strncpy(info.name,name,MaxName);
    strncpy(info.email,email,MaxEmail);
    strncpy(info.username,username,MaxUsername);
    strncpy(info.password,password,MaxPassword);
    fwrite(&info,sizeof(UserRegister), 1, file);
    fclose(file);
    printf("\n\n---You have successfully registered---");
    getchar();
}


//Request user to input login details
//if found, user will be linked to menu for user, else error message appear
void loginUser()
{
    int found=0;
    char username[MaxUsername] = {0};
    char password[MaxPassword] = {0};
    UserRegister info = {0};

    FILE *file = NULL;
    file = fopen(fileUser,"r");
    if(file == NULL)
    {
        printf("\n\n\n File is not opened\n");
        exit(1);
    }

    printf("\n\n\n Username:");
    scanf("%s", username);
    strncpy(sessionName,username,MaxUsername);
    printf("\n Password:");
    scanf("%s", password);
    while(fread (&info,sizeof(UserRegister), 1, file))
    {
      if((!strcmp(username,info.username)) && (!strcmp(password,info.password)))
      {
          found = 1;
          printf("\n\n---You have successfully login---");
          break;
      }
      else {
          found =0;
         }
   }

   if(found)
   {
    int choice = 0 ;

    do{
        printf("\n\n\n*************** MENU ***************");
        printf("\n\n\n1.Borrow Books");
        printf("\n2.Return Book");
        printf("\n3.Main Menu");
        printf("\n0.Exit");
        printf("\n\n\nEnter choice => ");
        scanf("%d",&choice);
        switch(choice)
        {
        case 1:
            borrow_book();
            break;
        case 2:
            return_book();
            break;
        case 3:
            mainMenu();
            break;
        case 0:
            printf("\n\n\n See you again \n\n\n\n\n");
            exit(1);
            break;
        default:
            printf("\n\n\n Invalid Input. Try again.");
        }
    }
    while ((choice=getchar())!= 0 );
    }
   else
   {
    printf("\nLogin Failed. Try again\n\n");
    }
 fclose(file);
}


//Request admin to input login details
//if username and password is admin,
//admin will be linked to menu for admin, else error message appear
void loginAdmin()
{
    int found=0;
    char username[MaxUsername] = {0};
    char password[MaxPassword] = {0};

    printf("\n\n\n Username:");
    scanf("%s", username);
    printf("\n Password:");
    scanf("%s", password);
    if((!strcmp(username,adminUsername)) && (!strcmp(password,adminUsername))){
         found = 1;
         printf("\n\n---Welcome Back Admin!---");
        }
    else {
        found = 0;
         }
   if(found)
   {
    int choice = 0;
    do{
       printf("\n\n\n*************** MENU ***************");
        printf("\n\n\n1.Add Books");
        printf("\n2.Delete Book");
        printf("\n3.Load All Books");
        printf("\n4.Search Book");
        printf("\n5.Main Menu");
        printf("\n0.Exit");
        printf("\n\n\nEnter choice => ");
        scanf("%d",&choice);
    switch(choice)
        {
       case 1:
            add_book(book);
            store_books(fileBook);
            break;
        case 2:
             remove_book(book);
            break;
        case 3:
            load_books(fileLibrary);
            break;
        case 4:
            findBooks();
            break;
	      case 5:
            mainMenu();
            break;
        case 0:
            printf("\n\n\n See you again \n\n\n\n\n");
            exit(1);
            break;
        default:
            printf("\n\n\n Invalid Input. Try again.");
        }
    }
   while ((choice=getchar())!= 0 );
   }
    else{
    printf("\n\n---Login Failed. Please Check ReadMe file to get details for Admin--");
   }
}


//main menu after welcome message appear
//if invalid input is being input, error message will appear
void mainMenu(){
    int choice = 0;
    do{
    printf("\n\n\n Please choose one");
    printf("\n\n\n1.Register");
    printf("\n2.Login");
    printf("\n3.Admin Mode");
    printf("\n4.Display All Books");
    printf("\n5.Search Book");
    printf("\n6.Exit");
    printf("\n\n\n Please enter a choice => ");
    scanf("%d",&choice);
    switch(choice)
        {
        case 1:
            printf("\n\n\n********** REGISTER HERE **********");
            registerUser();
            break;
        case 2:
            printf("\n\n\n********** USER LOGIN HERE **********");
            loginUser();
            break;
        case 3:
            printf("\n\n\n********** ADMIN LOGIN HERE **********");
            loginAdmin();
            break;
        case 4:
            load_books(fileLibrary);
            break;
        case 5:
            findBooks();
            break;
        case 6:
            printf("\n\n\n See you again \n\n\n\n\n");
            exit(1);
            break;
        default:
            printf("\n\n\n Invalid Input. Try again.");
        }
    }
    while ((choice=getchar())!= 0 );
}



int main()
{
    createFileUser();
    createFileLoan();
    headerBanner();
    mainMenu();

    return 0;
}
