#include "book_management.h"
#include "user_management.h"
#include <stdlib.h>
#include <string.h>

static struct Book book;    //declare book of type Book
static struct BookArray libArrays;    //declare libArrays of type BookArray


//saves all book in fileLibrary
//returns 0 if books were stored correctly, or an error code otherwise
int store_books(FILE *file)
{
    char bookTitle[MaxBookName] ;   //declare array for book title
    char nameAuthors[MaxAuthorName];    //declare array for author's name

    file = fopen(fileBook,"rb");
    if(file == NULL)
    {
        printf("File is not opened\n");
       exit(1);
    }

    FILE *ptr;
    ptr = fopen(fileLibrary,"ab");
    if(ptr == NULL)
    {
        printf("File is not opened\n");
        exit(1);
    }

    while(fread(&book,sizeof(struct Book), 1, file) ==1 && fread(bookTitle, sizeof(bookTitle), 1, file)==1 &&fread(nameAuthors, sizeof(nameAuthors), 1, file)==1)
    {
        book.title = bookTitle;
        book.authors = nameAuthors;
        printf("\n\n----Adding book to library ----");
        printf("\n\nTitle: %s", bookTitle);
        printf("Author: %s", nameAuthors);
        printf("Year: %i", book.year);
        printf("\nCopies: %i", book.copies);
        fwrite(&book,sizeof(struct Book), 1, ptr);
        fwrite(bookTitle, sizeof(bookTitle), 1, ptr);
        fwrite(nameAuthors, sizeof(nameAuthors), 1, ptr);
        printf("\n\n----Book successfully added to library ! ----");
    }
    fclose(file);
    fclose(ptr);

    return 0;
}



//loads the database of books from the fileLibrary
//returns 0 if books were loaded correctly, or an error code otherwise
int load_books(FILE *file)
{
    int id =1;
    char bookTitle[MaxBookName] ;
    char nameAuthors[MaxAuthorName];

    file = fopen(fileLibrary,"rb");
    if(file == NULL)
    {
        printf("File is not opened\n");
        exit(1);
    }

    while(fread(&book,sizeof(struct Book), 1, file) ==1 && fread(bookTitle, sizeof(bookTitle), 1, file)==1 &&fread(nameAuthors, sizeof(nameAuthors), 1, file)==1)
    {
        book.title = bookTitle;
        book.authors = nameAuthors;
        printf("\n\n\nBook ID : %i",id);
        printf("\nBook Title : %s",book.title);
        printf("Book Author : %s",book.authors);
        printf("Book Year : %i",book.year);
        printf("\nBook Copies : %i",book.copies);
        id++;
   }
    fclose(file);
    return 0;
}


// Get details and add a book to fileBook
//returns 0 if the book could be added, or an error code otherwise
int add_book(struct Book book)
{

    int c;    //declare variable for clearing input buffer
    char bookTitle[MaxBookName] = {0};    //declare array to store book title
    char nameAuthors[MaxAuthorName] = {0};    //declare array to store author's name
    book.title = bookTitle;
    book.authors = nameAuthors;

    FILE *file;
    file = fopen(fileBook,"wb");
    if(file == NULL)
    {
        printf("File is not opened\n");
        exit(1);
    }

    printf("\n\n\n********** ENTER BOOK DETAILS BELOW **********");
    printf("\nBook Tittle  = ");
    while ((c = getchar()) != '\n');
    fgets(bookTitle ,MaxBookName,stdin);
    strncpy(book.title, bookTitle,MaxBookName);
    printf("\nAuthor Name  = ");
    fgets(nameAuthors ,MaxAuthorName,stdin);
    strncpy(book.authors, nameAuthors, MaxAuthorName);
    printf("\nYear of Publication =  ");
    scanf("%u",&book.year);
    printf("\nNumber of Book Copies=  ");
    scanf("%u",&book.copies);

    fwrite(&book, sizeof(struct Book), 1, file);
    fwrite(bookTitle, sizeof(bookTitle), 1, file);
    fwrite(nameAuthors, sizeof(nameAuthors), 1, file);
    fclose(file);
    return 0;
}



//removes a book from the library by book title, author or year
//by creating a temporary file
//returns 0 if the book could be successfully removed, or an error code otherwise
int remove_book(struct Book book)
{
    int found=0;
    int borrowed=0;
    LoanStatus status = {0};
    char bookTitle[MaxBookName] ;
    char nameAuthors[MaxAuthorName];
    char bookName[MaxBookName] ;
    char authorName[MaxAuthorName];
    int bookyear;

    int choice = 0;
    printf("\n\n\n Please choose how you want to remove book");
    printf("\n\n\n1.By book title");
    printf("\n2.By book author");
    printf("\n3.By year");
    printf("\n0.Exit");
    printf("\n\n\n Please enter a choice => ");
    scanf("%d",&choice);
    switch(choice)
        {
        case 1:
            printf("\nEnter Book Title to remove: ");
            int c;
            while ((c = getchar()) != '\n' && c != EOF);
            fgets(bookName ,MaxBookName,stdin);
            break;
        case 2:
            printf("\nEnter Author's name to remove: ");
            while ((c = getchar()) != '\n' && c != EOF);
            fgets(authorName ,MaxAuthorName,stdin);
            break;
        case 3:
            printf("\nEnter Year of Publication to remove: ");
            scanf("%i",&bookyear);
            break;
        case 0:
            printf("\n\n\n See you again \n\n\n\n\n");
            exit(1);
            break;
        default:
            printf("\n\n\n Invalid Input. Try again.");
        }

    FILE *file;
    file = fopen(fileLibrary,"rb");

    if(file == NULL)
    {

        printf("File is not opened\n");
        exit(1);
    }

    FILE *ptr;
    ptr = fopen(fileLoan,"rb");
    if(ptr == NULL)
    {
        fclose(file);
        printf("File is not opened\n");
        exit(1);
    }

    FILE *tmpFp;
    tmpFp = fopen("replace.txt","wb");
    if(tmpFp == NULL)
    {
        fclose(file);
        printf("File is not opened\n");
        exit(1);
    }

    book.title = bookTitle;
    book.authors = nameAuthors;

                while(fread(&status,sizeof(LoanStatus), 1, ptr) ==1){

               if(!strcmp(status.bookTitle, bookName) || !strcmp(status.bookAuthor, authorName) || status.year ==bookyear ){
                   borrowed =1;
               }
            }

    while(fread(&book,sizeof(struct Book), 1, file) ==1 && fread(bookTitle, sizeof(bookTitle), 1, file)==1 &&fread(nameAuthors, sizeof(nameAuthors), 1, file)==1)
    {

         if(!strcmp(bookTitle, bookName)  && borrowed ==0 || !strcmp(nameAuthors, authorName) && borrowed ==0 || book.year == bookyear)
            {
                found =1;

            }
            else{
            fwrite(&book,sizeof(struct Book), 1, tmpFp);
            fwrite(bookTitle, sizeof(bookTitle), 1, tmpFp);
            fwrite(nameAuthors, sizeof(nameAuthors), 1, tmpFp);
            }
    }
    if(found==0 && borrowed==0)
    {
        printf("\n\n\nBook not found in library\n");

    }
    else if (borrowed==1) {
        printf("\n\n\nBook is on loan\n");
    }
    else if (found==1 && borrowed==0){
        printf("\n\n\nYou have successfully removed a book. \n");

    }
    else{
        printf("\n\n\nUnknown");
    }

    fclose(file);
    fclose(ptr);
    fclose(tmpFp);
    remove(fileLibrary);
    rename("replace.txt",fileLibrary);
}

//finds books published in the given year.
//returns a BookArray structure, where the field "array" is a newly allocated array of books, or null if no book with the
//provided title can be found. The length of the array is also recorded in the returned structure, with 0 in case
//array is the null pointer.
struct BookArray find_book_by_year (unsigned int year)
{
    char bookTitle[MaxBookName] ;
    char nameAuthors[MaxAuthorName];
    int found =0;
    FILE *file;
    file = fopen(fileLibrary,"rb");

    if(file == NULL)
    {
        printf("File is not opened\n");
        exit(1);
    }
    book.title = bookTitle;
    book.authors = nameAuthors;
    while(fread(&book,sizeof(struct Book), 1, file) ==1 && fread(bookTitle, sizeof(bookTitle), 1, file)==1 &&fread(nameAuthors, sizeof(nameAuthors), 1, file)==1)
    {
     if(book.year == year)
        {
        found = 1;
        libArrays.array = &book;
        book.authors = nameAuthors;
        book.title = bookTitle;
        printf("\nBook found!");
        printf("\n\nTitle: %s",libArrays.array->title);
        printf("Author: %s",libArrays.array->authors);
        printf("Copies: %i", libArrays.array->copies);
        printf("\nYear: %i",libArrays.array->year);

        }
    }
    if(!found){
        printf("\nBook not found!");
    }
    else{
      return libArrays;
    }
    fclose(file);
}


//finds books with the given authors.
//returns a BookArray structure, where the field "array" is a newly allocated array of books, or null if no book with the
//provided title can be found. The length of the array is also recorded in the returned structure, with 0 in case
//array is the null pointer.
struct BookArray find_book_by_author (const char *author)
{
    char bookTitle[MaxBookName] ;
    char nameAuthors[MaxAuthorName];
    int found =0;
    FILE *file;
    file = fopen(fileLibrary,"rb");

    if(file == NULL)
    {

        printf("File is not opened\n");
        exit(1);
    }
    book.title = bookTitle;
    book.authors = nameAuthors;
    while(fread(&book,sizeof(struct Book), 1, file) ==1 && fread(bookTitle, sizeof(bookTitle), 1, file)==1 &&fread(nameAuthors, sizeof(nameAuthors), 1, file)==1)
    {
     if(!strcmp(nameAuthors, author))
        {
         found = 1;
        libArrays.array = &book;
        book.authors = nameAuthors;
        book.title = bookTitle;
        printf("\nBook found!");
        printf("\n\nTitle: %s",libArrays.array->title);
        printf("Author: %s",libArrays.array->authors);
        printf("Copies: %i", libArrays.array->copies);
        printf("\nYear: %i",libArrays.array->year);
         return libArrays;
        }
    }
    if(!found){
        printf("\nBook not found!");
    }
    else{
      return libArrays;
    }

    fclose(file);
}


//finds books with a given title.
//returns a BookArray structure, where the field "array" is a newly allocated array of books, or null if no book with the
//provided title can be found. The length of the array is also recorded in the returned structure, with 0 in case
//array is the null pointer.
struct BookArray find_book_by_title (const char *title)
{
    int found =0;
    char bookTitle[MaxBookName] ;
    char nameAuthors[MaxAuthorName];

    FILE *file;
    file = fopen(fileLibrary,"rb");

    if(file == NULL)
    {

        printf("File is not opened\n");
        exit(1);
    }
    book.title = bookTitle;
    while(fread(&book,sizeof(struct Book), 1, file) ==1 && fread(bookTitle, sizeof(bookTitle), 1, file)==1 &&fread(nameAuthors, sizeof(nameAuthors), 1, file)==1)
    {
     if(!strcmp(bookTitle, title))
        {
        found = 1;
        libArrays.array = &book;
        book.authors = nameAuthors;
        book.title = bookTitle;
        printf("\n\nBook found!");
        printf("\nTitle: %s",libArrays.array->title);
        printf("Author: %s",libArrays.array->authors);
        printf("Copies: %i", libArrays.array->copies);
        printf("\nYear: %i",libArrays.array->year);
        }
    }
    if(!found){
        printf("\nBook not found!");
    }
    else{
      return libArrays;
    }
    fclose(file);

}
