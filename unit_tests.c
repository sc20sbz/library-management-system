#include "unity.h"
#include "knowlegde.h"



void  test_trim_trailing() {
  
  char input[100] = "AAAA       "; 
  char inputtwo[100] = " A112AAA       ";
  char inputthree[100] = "      A112AAA_      ";
  
  TEST_ASSERT_EQUAL_STRING("AAAA", trim_trailing(input));
  TEST_ASSERT_EQUAL_STRING(" A112AAA", trim_trailing(inputtwo));
  TEST_ASSERT_EQUAL_STRING("      A112AAA_", trim_trailing(inputthree));

}


void  test_convert_lowercase() {
  
  char input[100] = "AAAA"; 
  char inputtwo[100] = " A112AAA";
  char inputthree[100] = " A112z!!ZA";
  
  TEST_ASSERT_EQUAL_STRING("aaaa", convert_lowercase(input));
  TEST_ASSERT_EQUAL_STRING(" a112aaa", convert_lowercase(inputtwo));
  TEST_ASSERT_EQUAL_STRING(" a112z!!za", convert_lowercase(inputthree));

}


void  test_remove_punc() {
  
  char input[100] = "AAA!!!!A"; 
  char inputtwo[100] = " !A11?2AAA??";
  char inputthree[100] = " !Aaa11?2AAA??";
   
  TEST_ASSERT_EQUAL_STRING("AAAA", remove_punc(input));
  TEST_ASSERT_EQUAL_STRING("AAAA", remove_punc(inputtwo));
   TEST_ASSERT_EQUAL_STRING("AaaAAA", remove_punc(inputthree));
}

void  test_trim_leading() {
  
  char input[100] = "         AAA!!!!A"; 
  char inputtwo[100] = "           !A11?2AAA??";
  char inputthree[100] = "           !A11?2AAA              ";
  
  TEST_ASSERT_EQUAL_STRING("AAA!!!!A", trim_leading(input));
  TEST_ASSERT_EQUAL_STRING("!A11?2AAA??", trim_leading(inputtwo));
  TEST_ASSERT_EQUAL_STRING("!A11?2AAA              ",trim_leading(inputthree));
}

void test_exist_keyword() {
    int i;
    char input[100] = "aaa"; 
    char inputtwo[100] = " aaa";
    i = exist_keyword(input,inputtwo);
    TEST_ASSERT_TRUE(i==1);
}

void test_exist_reply() {
    int i,j;
    char * input[] = {"aaa"}; 
    char inputtwo[100] = " aaa";
  
    i = exist_reply(input,inputtwo);
    TEST_ASSERT_TRUE(i==1);
    
}



void setUp() {
	 //this function is called before each test, it can be empty
}

void tearDown() {
	 //this function is called after each test, it can be empty
}

int main() {
	UNITY_BEGIN();


	RUN_TEST(test_trim_trailing);
	RUN_TEST(test_convert_lowercase);
	RUN_TEST(test_remove_punc);
	RUN_TEST(test_trim_leading);
	RUN_TEST(test_exist_keyword);
    RUN_TEST(test_exist_reply);
	return UNITY_END();
}
