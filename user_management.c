#include "user_management.h"
#include "book_management.h"
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

static struct Book book;    //declare book of type Book
char sessionName[MaxName];    //declare current sessions user's name



//search book in fileLibrary by title
//if found, and not currently on loan
//user and book details is saved into struct LoanStatus
//struct LoanStatus will be written into the fileLoan if the book successfully borrowed
//library is being updated after borrow book, copies reduce by 1
void borrow_book()
{
    int c;    //declare variable for clearing input buffer
    int found=0;      //declare variable for found book
    int borrowed=0;     //declare variable for borrowed book
    LoanStatus status = {0};   //declare array to store book loan
    UserRegister info = {0};    //declare array to store user info
    char bookTitle[MaxBookName] ;  //declare array to store book Title
    char bookName[MaxBookName] ;      //declare array to store book name from user input
    char nameAuthors[MaxAuthorName];    //declare array to store book Title

    printf("\nEnter Book Title to borrow: ");
    while ((c = getchar()) != '\n' && c != EOF);
    fgets(bookName ,MaxBookName,stdin);

    FILE *k = NULL;
    k = fopen(fileUser,"rb");
    if(k == NULL)
    {
        printf("File is not opened\n");
        exit(1);
    }
    fread (&info,sizeof(fileUser), 1, k);

    FILE *file;
    file = fopen(fileLibrary,"rb");

    if(file == NULL)
    {
        printf("File is not opened\n");
        exit(1);
    }

    FILE *ptr;
    ptr = fopen(fileLoan,"ab+");
    if(ptr == NULL)
    {
        fclose(file);
        printf("File is not opened\n");
        exit(1);
    }

    FILE *tmpFp;
    tmpFp = fopen("replace.txt","wb");
    if(tmpFp == NULL)
    {
        fclose(file);
        printf("File is not opened\n");
        exit(1);
    }

    book.title = bookTitle;
    book.authors = nameAuthors;

  while(fread(&status,sizeof(LoanStatus), 1, ptr) ==1){

    if(!strcmp(status.bookTitle, bookName) && !strcmp(status.name, sessionName))
    {
      borrowed =1;
    }
  }

  while(fread(&book,sizeof(struct Book), 1, file) ==1 && fread(bookTitle, sizeof(bookTitle), 1, file)==1 &&fread(nameAuthors, sizeof(nameAuthors), 1, file)==1)
  {
    if(!strcmp(bookTitle, bookName) && borrowed ==0 && book.copies>0)
    {
        found =1;
        printf("\nName: %s", sessionName);
        printf("\nTitle: %s", bookTitle);
        printf("Author: %s", nameAuthors);
        printf("Copies: %i", book.copies);
        printf("\nYear: %i", book.year);
        strncpy(status.name, sessionName, MaxName);
        book.copies -- ;
        status.year = book.year;
        strncpy(status.bookTitle, bookTitle, MaxBookName);
        strncpy(status.bookAuthor, nameAuthors, MaxAuthorName);
        fwrite(&status,sizeof(LoanStatus), 1, ptr);
        fwrite(&book,sizeof(struct Book), 1, tmpFp);
        fwrite(bookTitle, sizeof(bookTitle), 1, tmpFp);
        fwrite(nameAuthors, sizeof(nameAuthors), 1, tmpFp);
      }
      else{
        fwrite(&book,sizeof(struct Book), 1, tmpFp);
        fwrite(bookTitle, sizeof(bookTitle), 1, tmpFp);
        fwrite(nameAuthors, sizeof(nameAuthors), 1, tmpFp);
        }
    }
    if(found==0 && borrowed==0)
    {
        printf("\n\nBook not found in library\n");
    }
    else if (borrowed==1) {
        printf("\n\nBook has been borrowed\n");
    }
    else if (found==1 && borrowed==0){
        printf("\n\n\nYou have successfully borrowed a book. \n");
    }
    else{
        printf("\n\n\nUnknown");
    }

    fclose(k);
    fclose(file);
    fclose(ptr);
    fclose(tmpFp);
    remove(fileLibrary);
    rename("replace.txt",fileLibrary);
}



//search book in fileLoan by title
//if found, user can return book
//restrict user to return the same book more than once
//The returned book will be removed from fileLoan
//library is being updated after return book, copies increase by 1
void return_book()
{
    int c;    //declare variable for clearing input buffer
    int found=0;      //declare variable for found book
    LoanStatus status = {0};   //declare array to store book loan
    char bookTitle[MaxBookName] ;  //declare array to store book Title
    char bookName[MaxBookName] ;      //declare array to store book name from user input
    char nameAuthors[MaxAuthorName];    //declare array to store book Title

    printf("\nEnter Book Title to return: ");
    while ((c = getchar()) != '\n' && c != EOF);
    fgets(bookName ,MaxBookName,stdin);

    FILE *file;
    file = fopen(fileLibrary,"rb");

    if(file == NULL)
    {
        printf("File is not opened\n");
        exit(1);
    }

    FILE *ptr;
    ptr = fopen(fileLoan,"rb");
    if(ptr == NULL)
    {
        fclose(file);
        printf("File is not opened\n");
        exit(1);
    }

    FILE *tmpFp;
    tmpFp = fopen("replace.txt","wb");
    if(tmpFp == NULL)
    {
        fclose(file);
        printf("File is not opened\n");
        exit(1);
    }

    FILE *newfile;
    newfile = fopen("newfile.txt","wb");
    if(newfile == NULL)
    {
        fclose(file);
        printf("File is not opened\n");
        exit(1);
    }

    book.title = bookTitle;
    book.authors = nameAuthors;
    while(fread(&book,sizeof(struct Book), 1, file) ==1 && fread(bookTitle, sizeof(bookTitle), 1, file)==1
          &&fread(nameAuthors, sizeof(nameAuthors), 1, file)==1)
    {
      while(fread(&status,sizeof(LoanStatus), 1, ptr) ==1)
      {
        if(!strcmp(status.bookTitle, bookName) && !strcmp(status.name, sessionName))
          {
          found =1;
          printf("\n Book found ");
          printf("\nName: %s", sessionName);
          printf("\nYear: %i", book.year);
          printf("\nCopies: %i", book.copies);
          printf("\nTitle: %s", bookName);
          printf("\n\n\nYou have successfully returned a book. \n");
          }
        else{
          fwrite(&status,sizeof(LoanStatus), 1, newfile);
            }
        }
      if(strcmp(bookTitle, bookName))
      {
          fwrite(&book,sizeof(struct Book), 1, tmpFp);
          fwrite(bookTitle, sizeof(bookTitle), 1, tmpFp);
          fwrite(nameAuthors, sizeof(nameAuthors), 1, tmpFp);
      }
     else {
          book.copies ++ ;
          fwrite(&book,sizeof(struct Book), 1, tmpFp);
          fwrite(bookTitle, sizeof(bookTitle), 1, tmpFp);
          fwrite(nameAuthors, sizeof(nameAuthors), 1, tmpFp);
         }
    }
    if(found==0)
    {
        printf("\n\n\nBook not found\n");
    }
    fclose(file);
    fclose(ptr);
    fclose(tmpFp);
    fclose(newfile);
    remove(fileLibrary);
    rename("replace.txt",fileLibrary);
    remove(fileLoan);
    rename("newfile.txt",fileLoan);
    printf("\n\n");
}
